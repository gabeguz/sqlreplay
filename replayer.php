#!/usr/bin/php

<?php
class SlowLogPlayer
{
    private static $_mysqli;
    private static $_logDir;
    private static $_sourceLogPath;
    private static $_sourceLogHandle;
    private static $_hitsLogName;
    private static $_hitsLogHandle;
    private static $_generalLogName;
    private static $_generalLogHandle;
    private static $_searchArray;
    private static $_config;
    private static $_debugLevel;

    private $_currentDb;
    private $_currentLine;
    private $_currentLineNum;
    private $_hitCount;
    private $_resultHandle;
    private $_startTime;
    private $_endTime;

    /**
     * Setup 
     * 
     * @param mixed $options 
     * @return void
    */
    public function __construct($options) {
        $this->_startTime            = time();
        $this->_config['logdir']     = isset($options['logdir']) ? $options['logdir'] : './logs';
        $this->_config['searchtext'] = isset($options['searchtext']) ? $options['searchtext'] : NULL;
        $this->_config['curdate']    = isset($options['curdate']) ? $options['curdate'] : NULL;
        $this->_config['file']       = isset($options['file']) ? $options['file'] : NULL;
        $this->_config['user']       = isset($options['user']) ? $options['user'] : NULL;
        $this->_config['password']   = isset($options['password']) ? $options['password'] : NULL;
        $this->_config['host']       = isset($options['host']) ? $options['host'] : NULL;
        $this->_config['database']   = isset($options['database']) ? $options['database'] : NULL;
        $this->_config['verbose']    = isset($options['verbose']) ? 1 : 0;
        $this->_sourceLogPath        = realpath($options['file']);
        $this->_generalLogName       = basename($options['file']);
        $this->_hitsLogName          = 'hits.txt';
        $this->_initLogging();
        $this->_initDb();
        $this->_initSource();
        $this->_initSearchPatterns();
        $this->_currentLine = 0;
        $this->_hitCount    = 0;
    }

    /**
     * Tear down
     * 
     * @return void
    */
    public function __destruct() {
        // close file handles, mysqli connections, etc
        $this->_endTime = time();
        $this->_log("finished parsing: " . $this->_config['file']);
        $this->_displayStats();
        fclose($this->_hitsLogHandle);
        fclose($this->_generalLogHandle);
        $this->_mysqli->close();
    }

    /**
     * Display some statistics
     * 
     * @return void
    */
    private function _displayStats() {
        printf("\n======\n");
        printf("Start:\t%s\n", date(DATE_ATOM, $this->_startTime));
        printf("End:\t%s\n", date(DATE_ATOM, $this->_endTime));
        printf("Lines:\t%d\n", $this->_currentLineNum);
        printf("Hits:\t%d\n", $this->_hitCount);
    }

    /**
     * Initialize logging facilities
     * 
     * @return void
    */
    private function _initLogging() {
        $this->_debugLevel = $this->_config['verbose'];
        if (! is_dir($this->_config['logdir'])) {
            mkdir($this->_config['logdir'], 0750, true);
        }
        $fullLogPath = realpath($this->_config['logdir']);
        $this->_hitsLogHandle = fopen($fullLogPath . '/' . $this->_hitsLogName, 'a');
        $this->_generalLogHandle = fopen($fullLogPath . '/' . $this->_generalLogName, 'a');
    }

    /**
     * Initialize database facilities
     * 
     * @return void
    */
    private function _initDb() {
        $this->_mysqli = new mysqli($this->_config['host'], $this->_config['user'], $this->_config['password'], $this->_config['database']);
        if($this->_mysqli->connect_error) {
            throw new Exception('Connect Error(' . $this->_mysqli->connect_errno . ') ' . $this->_mysqli->connect_error);
        }
        if($this->_config['database'] != NULL ) {
            $this->_currentDb = 'use ' . $this->_config['database'];
        }
    }

    /**
     * Initialize source file
     * 
     * @return void
    */
    private function _initSource() {
        if(file_exists($this->_sourceLogPath)) {
            $this->_sourceLogHandle = fopen($this->_sourceLogPath, 'r');
        } else {
            throw new Exception("No such file: $this->_sourceLogPath");
        }
    }

    /**
     * Initialize search patterns
     * 
     * @return void
    */
    private function _initSearchPatterns() {
        if(strlen($this->_config['searchtext']) > 0) {
            $this->_searchArray = explode(',', $this->_config['searchtext']);
        } else {
            $this->_searchArray = array('test@hotmail.com', 'test2@hotmail.com');
        }
    }

    /**
     * Handle logging to files
     * 
     * @param mixed $msg 
     * @param string $type 
     * @return void
    */
    private function _log($msg, $type = 'general') {
        $msg = sprintf("%s\t%s\n", date(DATE_ATOM), $msg);
        switch ($type) {
            case "hits":
                fwrite($this->_hitsLogHandle, $msg); 
                break;
            case "debug":
                if($this->_debugLevel < 1) {
                    break;
                }
            default:
                fwrite($this->_generalLogHandle, $msg); 
                break;
        }
    }

    /**
     * Preprocess the query, ignore blank lines, change CURDATE() calls if 
     *  needed
     * 
     * @return void
    */
    private function _prepLineForQuery() {
        $this->_currentLine = trim($this->_currentLine);
        if(strlen($this->_currentLine) < 1) {
            $this->_log("Blank line", 'debug');
            return false;
        }

        if(preg_match('/^--/i', $this->_currentLine) > 0) {
            // a comment return false
            $this->_log("Comment: $this->_currentLine", 'debug');
            return false;
        }

        if(preg_match('/^SELECT GET_LOCK\(\'cacti_monitoring\', 300\) AS ok/', $this->_currentLine) > 0) {
            $this->_log('Skip GET_LOCK(\'cacti_monitoring\') call', 'debug');
            return false;
        }

        if(preg_match('/^use/i', $this->_currentLine) > 0) {
            // a db change statement execute it, and see the results
            //  if it works, set _currentDb to this value, if not...
            //  leave _currentDb as is.
            if ($this->_mysqli->query($this->_currentLine)) {
                $this->_log("Switch db: $this->_currentLine", 'debug');
                $this->_currentDb = $this->_currentLine;
            } else {
                $this->_log("Could not switch to db: $this->_currentLine", 'debug');
            }
            return false;
        }

        if($this->_config['curdate'] !== NULL) {
            if(preg_match('/curdate\(\)/i', $this->_currentLine) || preg_match('/current_date\(\)/i', $this->_currentLine)) {
                // search for an occurence of CURDATE in the line, and replace
                $this->_currentLine = preg_replace('/curdate\(\)/i', "DATE('" . $this->_config['curdate'] . "')", $this->_currentLine);
                $this->_currentLine = preg_replace('/current_date\(\)/i', "DATE('" . $this->_config['curdate'] . "')", $this->_currentLine);
                $msg = 'Replaced CURDATE() with ' . $this->_config['curdate'];
                $this->_log($msg, 'debug');
            }
        }
    }

    /**
     * Run the current query
     * 
     * @return void
    */
    private function _query() {
        $this->_log("Current Line = $this->_currentLine", 'debug');
        if($result = $this->_mysqli->query($this->_currentLine)) {
            if($result instanceof MySQLi_Result) {
                $this->_resultHandle = $result;
                return true;
            } else {
                $this->_log("Result for query $this->_currentLine is not an instance of MySQLi_Result", 'debug');
                return false;
            }
        } else {
            $this->_log("Query $this->_currentLine could not be processed", 'debug');
            return false;
        }
    }

    /**
     * Parse the current query results, search for hits 
     * 
     * @return void
    */
    private function _searchResults() {
        $this->_log("Searching...", 'debug');
        while($row = $this->_resultHandle->fetch_assoc()) {
            foreach($row as $key=>$haystack) {
                foreach($this->_searchArray as $needle) {
                    $this->_log("Needle: $needle \t Haystack: $haystack", 'debug');
                    if(stristr($haystack, $needle)) {
                        $msg = sprintf("--Found %s on line %d in file %s. Query:\n%s\n%s\n", $needle, $this->_currentLineNum, $this->_sourceLogPath, $this->_currentDb, $this->_currentLine);
                        echo $msg;
                        $this->_hitCount++;
                        $this->_log($msg);
                        $this->_log($msg, 'hits');
                    }
                }
            }
        }
    }

    /**
     * Play the logfile, search results 
    */
    public function playAndParse() {    
        $this->_log("started parsing: " . $this->_config['file']);
        while ($this->_currentLine = fgets($this->_sourceLogHandle)) {
            $this->_currentLineNum++;
            if($this->_prepLineForQuery() === false) {
                continue;
            } else {
                if ($this->_query() === false) {
                    continue;
                } else {
                    $this->_searchResults();
                }
            }
        }
    }
}

// main
$shortopts = "";
$longopts = array(
                  "file:",
                  "logdir:",
                  "searchtext:",
                  "curdate:",
                  "verbose",
                  "user:",
                  "password:",
                  "database:",
                  "host:",
                  "help"
                 );
$options = getopt($shortopts, $longopts);

if(isset($options['help'])) {
    printf("--file name \t the session log to replay\n");
    printf("--logdir name \t the directory to write logs to\n");
    printf("--searchtext string[,string] \t a string, or comma seperated list of strings to search for in the query results\n");
    printf("--curdate date \t a mysql formatted date (2011-09-11) string to replace occurences of CURDATE() in the replayed queries\n");
    printf("--verbose \t include debug messages in general log\n");
    printf("--user user \t mysql user to connect as\n");
    printf("--password password \t mysql password for user\n");
    printf("--host host \t mysql host to connect to\n");
    printf("--database database \t database to connect to\n");
    printf("--help \t This message\n");
    exit(0);
}

if(isset($options['file'])) {
    $replayer = new SlowLogPlayer($options);
    $replayer->playAndParse();
} else {
    printf("Missing --file parameter\n");
    exit(0);
}
